const isAnagram = require('./anagram');

test('Word: should match', () => {
    expect(isAnagram('bleat', 'table')).toBe(true);
});

test('Word: should not match', () => {
    expect(isAnagram('earth', 'throw')).toBe(false);
});

test('Word: should not match 2', () => {
    expect(isAnagram('mouse', 'mist')).toBe(false);
});

test('Word: check case sensitivity', () => {
    expect(isAnagram('LiStEn', 'sileNT')).toBe(true);
});

test('Sentence: should match', () => {
    expect(isAnagram('Tom Marvolo Riddle', 'I am lord Voldemort')).toBe(true);
});

test('Number and word input: should throw error', () => {
    expect(isAnagram.bind(null, 'abc', 100)).toThrow();
});