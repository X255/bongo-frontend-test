function isAnagram(string1, string2) {
    if (typeof string1 !== 'string' || typeof string2 !== 'string') {
        throw new Error('Invalid input.');
    }
    if (string1 === string2) {
        return true;
    }
    string1 = cleanString(string1);
    string2 = cleanString(string2);
    if (string1.length !== string2.length) {
        return false;
    }
    else {
        const array1 = toSortedArray(string1);
        const array2 = toSortedArray(string2);
        return array1.every((char, index) => {
            return array2[index] === char;
        })
    }
}

function cleanString(string) {
    return string.replace(/[^a-zA-Z0-9]/gi, '');
}

function toSortedArray(string) {
    return string.trim().toLowerCase().replace(/[^a-z0-9]/gi, '').split('').sort();
}

/** complexity:
 * best case: O(1) when same string or different lengths
 * worst case: O(n) when different strings
 */

module.exports = isAnagram;