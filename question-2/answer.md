This type of pattern is the Factory Method, as this object has abstract methods for classes. The Vehicle interface allows the creation of related objects without specifying their exact classes.

### Answer 2A
Car and Plane classes can be implemented by use of inheritance concept of OOP with `implements` keyword as shown below:
```
class Car implements Vehicle {
    @Override
    int set_num_of_wheels() {
        return 4;
    }
    @Override
    int set_num_of_passengers() {
        return 5;
    }
    @Override
    boolean has_gas() {
        return true;
    }
}

class Plane implements Vehicle {
    @Override
    int set_num_of_wheels() {
        return 10;
    }
    @Override
    int set_num_of_passengers() {
        return 100;
    }
    @Override
    boolean has_gas() {
        return false;
    }
}
```

### Answer 2B
Previous solution done with *Facade* pattern.
```
class Car implements Vehicle {
    @Override
    int set_num_of_wheels() {
        return 4;
    }
    @Override
    int set_num_of_passengers() {
        return 5;
    }
    @Override
    boolean has_gas() {
        return true;
    }
}
class Plane implements Vehicle {
    @Override
    int set_num_of_wheels() {
        return 10;
    }
    @Override
    int set_num_of_passengers() {
        return 100;
    }
    @Override
    boolean has_gas() {
        return false;
    }
}

public class VehicleGenerator {
    private Vehicle car;
    private Vehicle plane;

    public VehicleGenerator() {
        car = new Car();
        plane = new Plane();
    }

    public int setCarWheel() {
        return car.set_num_of_wheels();
    }
    public int setPlaneWheel() {
        return plane.set_num_of_wheels();
    }
    
    /* ...repeat for all functions of each class */
}
```
