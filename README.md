# Bongo Frontend Test

This repository holds solutions to the Bongo Frontend Test Code.
Answers to each question are provided in their respective folders

## Requirements

- OS: Windows 10
- Software: NodeJS v14+, VSCode

## Notes
- Question 1 and 3 need to be installed before running.
- Question 2 answer is given in both Markdown & text file.

## Installation
- In `question-1`, folder run `npm install` in the command line.
- In `question-3`, folder run `npm install` in the command line.

## Running Scripts
- To test anagram function, in `question-1` folder,  run `npm run test` in the command line.
- In `question-3`, folder run `npm start` in the command line.
- To test video player, in `question-3` folder,  run `npm run test` in the command line.

## Libraries used
- Jest
- React
- Bootstrap 5