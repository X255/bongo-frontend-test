import './styles/styles.css';
import VideoPlayer from './components/VideoPlayer';

function App() {
	return (
		<div className="App App-header">
			<header className="mb-5">
				<h3>Video Player</h3>
			</header>
			<main>
				<VideoPlayer />
			</main>
		</div>
	);
}

export default App;
