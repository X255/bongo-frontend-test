import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import VideoPlayer from '../VideoPlayer';

describe('video player test', ()=>{
    beforeEach(() => {
        render(<VideoPlayer />);
    });
    test('shows URL form on initial load', () => {
        const linkElement = screen.getByText(/submit/i);
        expect(linkElement).toBeInTheDocument();
    });
    test('show video player', async () => {
        const linkElement = screen.getByText(/submit/i);
        fireEvent.click(linkElement)
        const video = screen.getByTestId('player');
        await waitFor(() => video);
        expect(video).toBeInTheDocument();
    });
})