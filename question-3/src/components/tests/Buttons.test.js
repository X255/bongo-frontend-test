import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import VideoPlayer from '../../App';

describe('video player test', () => {
    beforeEach(async () => {
        render(<VideoPlayer />);
        const linkElement = screen.getByText(/submit/i);
        fireEvent.click(linkElement)
        const video = screen.getByTestId('player');
        await waitFor(() => video);
    });

    test('video player forward', () => {
        const forward = screen.getByText(/forward/i);
        fireEvent.click(forward)
        const video = screen.getByTestId('player');
        expect(video).toHaveProperty('currentTime', 5);
    });

    test('video player forward first, then rewind', () => {
        const forward = screen.getByText(/forward/i);
        const rewind = screen.getByText(/rewind/i);
        fireEvent.click(forward);
        fireEvent.click(rewind);
        const video = screen.getByTestId('player');
        expect(video).toHaveProperty('currentTime', 0);
    });

    test('play video for 3 seconds, then pause', () => {
        const play = screen.getByTestId('play-btn');
        fireEvent.click(play);
        setTimeout(() => {
            fireEvent.click(play);
            const video = screen.getByTestId('player');
            expect(video).toHaveProperty('currentTime', 3);
        }, 3000);
    });
})