export default function Play({ togglePlay, isPlaying }) {
    return (
        <button className="btn btn-outline-light" onClick={togglePlay} data-testid="play-btn">
            <i className={`bi bi-${isPlaying ? 'pause' : 'play'} d-block`}></i>
            {isPlaying ? 'PAUSE' : 'PLAY'}
        </button>
    )
}
