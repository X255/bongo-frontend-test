export default function Forward({ onForward }) {
    return (
        <button className="btn btn-outline-light" onClick={onForward}>
            <i className="bi bi-skip-forward d-block"></i>
            FORWARD
        </button>
    )
}
