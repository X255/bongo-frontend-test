import { useState, useRef, useMemo, useEffect } from 'react';
import Play from './Play';
import Forward from './Forward';
import Rewind from './Rewind';

export default function VideoPlayer() {
    const [url, setUrl] = useState(`http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4`);
    const [hasVideo, toggleVid] = useState(false);
    const [isPlaying, togglePlay] = useState(false);
    const [progress, setProgress] = useState(0);
    const videoElem = useRef(null);

    useMemo(() => {
        isPlaying ? videoElem?.current?.play() : videoElem?.current?.pause();
    }, [isPlaying])

    useEffect(() => {
        if (hasVideo && videoElem.current) {
            videoElem.current.addEventListener('timeupdate', onTimeUpdate)
            videoElem.current.addEventListener('ended', onEnd)
        }
        else {
            togglePlay(false);
            setProgress(0);
            videoElem?.current?.removeEventListener('timeupdate', onTimeUpdate)
            videoElem?.current?.removeEventListener('ended', onEnd)
        }
    }, [hasVideo, videoElem])

    function showHidePlayer() {
        toggleVid(p => !p)
    }

    function rewindFn() {
        videoElem.current.currentTime -= 5;
    }

    function forwardFn() {
        videoElem.current.currentTime += 5;
    }

    function onTimeUpdate(e) {
        const { current } = videoElem;
        if (!current) {
            return;
        }
        const pct = ((current.currentTime / current.duration) * 100);
        setProgress(pct);
    }

    function onEnd() {
        togglePlay(false);
    }

    const playerBox = (
        <>
            <div className="mb-3">
                <video ref={videoElem} width={500} height={282} data-testid="player"  >
                    <source src={url} />
                </video>
                <div className="progress" style={{ height: "2px" }}>
                    <div className="progress-bar" role="progressbar" style={{ width: `${progress}%` }}></div>
                </div>
            </div>
            <div className="mb-4">
                <div className="btn-group">
                    <Rewind onRewind={rewindFn} />
                    <Play isPlaying={isPlaying} togglePlay={() => { togglePlay(p => !p) }} />
                    <Forward onForward={forwardFn} />
                </div>
            </div>
            <button className="btn btn-outline-info" onClick={showHidePlayer}>
                Change Video Source
            </button>
        </>
    );

    const videoForm = (
        <>
            <div className="row mb-3 g-3 align-items-center">
                <div className="col-auto">
                    <label htmlFor="inp" className="col-form-label">Video URL:</label>
                </div>
                <div className="col-auto">
                    <input type="text" id="inp" className="form-control" value={url} onChange={(e) => { setUrl(e.currentTarget.value) }} />
                </div>
                <div className="col-auto">
                    <button className="btn btn-light" onClick={showHidePlayer}>
                        Submit
                    </button>
                </div>
            </div>
            <div className=''>
                Looking for video URLs? <a href="https://gist.github.com/jsturgis/3b19447b304616f18657" target="_blank" rel="noreferrer noopener">Visit this page</a>
            </div>
        </>
    )

    return (
        <div>
            {hasVideo ? playerBox : videoForm}
        </div>
    )
}