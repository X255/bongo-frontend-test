export default function Rewind({ onRewind }) {
    return (
        <button className="btn btn-outline-light" onClick={onRewind}>
            <i className="bi bi-skip-backward d-block"></i>
            REWIND
        </button>
    )
}
