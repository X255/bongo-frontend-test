import { render, screen } from '@testing-library/react';
import App from './App';

test('initial display', () => {
  render(<App />);
  const linkElement = screen.getByText(/video player/i);
  expect(linkElement).toBeInTheDocument();
});