This app has been bootstrapped with create-react-app

## Pseudo-code
```
VideoPlayer component:
    if(!videoLoaded):
        display video url prompt
    else:
        display video player

Play button:
    if(playing):
        display pause button
    else:
        display play button

Forward button:
    skip forward 5 seconds

Rewind button:
    skip backward 5 seconds
```

## Design pattern
Play, Rewind & Forward components are implemented as stateless React components.
